import React from 'react';

const Card = (props) => {

  const suits = {
    S: ['spades', '♠'], H: ['hearts', '♥'], C: ['clubs', '♣'], D: ['diams', '♦']
  };

  let myClassName = `card rank-${(props.rank).toLowerCase()} ${suits[props.suit][0]}`;

  return(
    <div className = {myClassName}>
      <span className="rank">{props.rank}</span>
      <span className="suit">{suits[props.suit][1]}</span>
    </div>
  )
};

export default Card;