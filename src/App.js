import React, { Component } from 'react';
import './App.css';
import Card from './Card/Card';
import WinCalculator from "./WinCalculator";

class App extends Component {
  ranks = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A'];
  suits = ['S', 'C', 'H', 'D'];

  state = {
    cards: [],
    outcome: ''
  };

  randomCards = () => {
    let cards = [];
    let counter = 5;
    while (counter > 0) {
      let random = Math.floor(Math.random() * this.ranks.length);
      let random2 = Math.floor(Math.random() * this.suits.length);
      let card = {rank: this.ranks[random], suit: this.suits[random2]};
      if (cards.findIndex(item => item.rank === card.rank && item.suit === card.suit) === -1) {
        cards.push(card);
        counter--;
      }
    }

    // console.log(cards);

    const calc = new WinCalculator(cards);
    const outcome = calc.getOutcome();

    this.setState({cards, outcome});
  };

  render() {
    return (
      <div className="App playingCards faceImages cards">
        <div><button className='App-btn' onClick={this.randomCards}>Shuffle Cards</button></div>
        <ul className="table">
          {
            this.state.cards.map((card, index) => {
              return (
                <li key={index}>
                  <Card suit={card.suit} rank={card.rank} />
                </li>
              )
            })
          }
        </ul>
        <div style={{marginTop: '20px'}}>{this.state.outcome}</div>
      </div>
    );
  }
}

export default App;
