import WinCalculator, { OUTCOMES } from "./WinCalculator";

const royalFlush = [
  {suit: 'H', rank: 'K'},
  {suit: 'H', rank: 'Q'},
  {suit: 'H', rank: 'J'},
  {suit: 'H', rank: '10'},
  {suit: 'H', rank: 'A'}
];

const flush = [
  {suit: 'H', rank: 'K'},
  {suit: 'H', rank: '2'},
  {suit: 'H', rank: '6'},
  {suit: 'H', rank: '10'},
  {suit: 'H', rank: 'A'}
];

const pair = [
  {suit: 'H', rank: '2'},
  {suit: 'S', rank: '2'},
  {suit: 'C', rank: 'J'},
  {suit: 'H', rank: 'K'},
  {suit: 'D', rank: 'A'}
];

const four = [
  {suit: 'H', rank: '3'},
  {suit: 'S', rank: '3'},
  {suit: 'C', rank: '3'},
  {suit: 'D', rank: '3'},
  {suit: 'S', rank: 'A'}
];

const three = [
  {suit: 'H', rank: '3'},
  {suit: 'S', rank: '3'},
  {suit: 'C', rank: '3'},
  {suit: 'D', rank: 'Q'},
  {suit: 'H', rank: 'J'}
];

const fullHouse = [
  {suit: 'H', rank: '3'},
  {suit: 'S', rank: '3'},
  {suit: 'C', rank: '3'},
  {suit: 'D', rank: 'A'},
  {suit: 'S', rank: 'A'}
];

const straight = [
  {suit: 'H', rank: '5'},
  {suit: 'S', rank: '4'},
  {suit: 'C', rank: '3'},
  {suit: 'D', rank: '2'},
  {suit: 'C', rank: 'A'}
];

const straightFlush = [
  {suit: 'S', rank: '5'},
  {suit: 'S', rank: '4'},
  {suit: 'S', rank: '3'},
  {suit: 'S', rank: '2'},
  {suit: 'S', rank: 'A'}
];

const twoPair = [
  {suit: 'H', rank: '3'},
  {suit: 'S', rank: '10'},
  {suit: 'C', rank: '3'},
  {suit: 'D', rank: 'A'},
  {suit: 'C', rank: '10'}
];

it('should determine royal flash', () => {
  const calc = new WinCalculator(royalFlush);
  const result = calc.getOutcome();

  expect(result).toEqual(OUTCOMES.ROYAL_FLUSH);
});

it('should determine flash', () => {
  const calc = new WinCalculator(flush);
  const result = calc.getOutcome();

  expect(result).toEqual(OUTCOMES.FLUSH);
});

it('should determine a pair', () => {
  const calc = new WinCalculator(pair);
  const result = calc.getOutcome();

  expect(result).toEqual(OUTCOMES.PAIR);
});

it('should determine four of a kind', () => {
  const calc = new WinCalculator(four);
  const result = calc.getOutcome();

  expect(result).toEqual(OUTCOMES.FOUR);
});

it('should determine full house', () => {
  const calc = new WinCalculator(fullHouse);
  const result = calc.getOutcome();

  expect(result).toEqual(OUTCOMES.FULL_HOUSE);
});

it('should determine three of a kind', () => {
  const calc = new WinCalculator(three);
  const result = calc.getOutcome();

  expect(result).toEqual(OUTCOMES.THREE);
});

it('should determine straight', () => {
  const calc = new WinCalculator(straight);
  const result = calc.getOutcome();

  expect(result).toEqual(OUTCOMES.STRAIGHT);
});

it('should determine straight flush', () => {
  const calc = new WinCalculator(straightFlush);
  const result = calc.getOutcome();

  expect(result).toEqual(OUTCOMES.STRAIGHT_FLUSH);
});