export const OUTCOMES = {
  ROYAL_FLUSH: 'Royal Flush',
  FLUSH: 'Flush',
  PAIR: 'A Pair',
  FOUR: 'Four of a kind',
  THREE: 'Three of a kind',
  FULL_HOUSE: 'Full House',
  TWO_PAIR: 'Two Pair',
  STRAIGHT: 'Straight',
  STRAIGHT_FLUSH: 'Straight Flush',
  NOTHING: 'Nothing'
};

class WinCalculator {
  constructor(cards) {
    this.cards = cards;

    this.suits = this.cards.map(card => card.suit);
    this.ranks = this.cards.map(card => card.rank);

    this.isFlush = this.suits.every(suit => suit === this.suits[0]);
  }

  isRoyalFlush() {
    return this.isFlush &&
      this.ranks.includes('10') &&
      this.ranks.includes('J') &&
      this.ranks.includes('Q') &&
      this.ranks.includes('K') &&
      this.ranks.includes('A');
  }

  isPair() {
    const ranksNumber = {};

    this.ranks.forEach(rank => {
      if (!ranksNumber[rank]) {
        ranksNumber[rank] = 1;
      } else ranksNumber[rank]++;
    });

    // console.log(Object.values(ranksNumber));

    return Object.values(ranksNumber).includes(2) &&
      Object.values(ranksNumber).includes(1);
  }

  isFour() {
    const ranksNumber = {};

    this.ranks.forEach(rank => {
      if (!ranksNumber[rank]) {
        ranksNumber[rank] = 1;
      } else ranksNumber[rank]++;
    });

    return Object.values(ranksNumber).includes(4);
  }

  isThree() {
    const ranksNumber = {};

    this.ranks.forEach(rank => {
      if (!ranksNumber[rank]) {
        ranksNumber[rank] = 1;
      } else ranksNumber[rank]++;
    });

    return Object.values(ranksNumber).includes(3) &&
      Object.values(ranksNumber).includes(1);
  }

  isStraight() {
    return (
      this.ranks.includes('10') &&
      this.ranks.includes('J') &&
      this.ranks.includes('Q') &&
      this.ranks.includes('K') &&
      this.ranks.includes('A')
      ) || (
      this.ranks.includes('9') &&
      this.ranks.includes('10') &&
      this.ranks.includes('J') &&
      this.ranks.includes('Q') &&
      this.ranks.includes('A')
      ) || (
      this.ranks.includes('8') &&
      this.ranks.includes('9') &&
      this.ranks.includes('10') &&
      this.ranks.includes('J') &&
      this.ranks.includes('A')
    ) || (
      this.ranks.includes('7') &&
      this.ranks.includes('8') &&
      this.ranks.includes('9') &&
      this.ranks.includes('10') &&
      this.ranks.includes('A')
    ) || (
      this.ranks.includes('6') &&
      this.ranks.includes('7') &&
      this.ranks.includes('8') &&
      this.ranks.includes('9') &&
      this.ranks.includes('A')
    ) || (
      this.ranks.includes('5') &&
      this.ranks.includes('6') &&
      this.ranks.includes('7') &&
      this.ranks.includes('8') &&
      this.ranks.includes('A')
    ) || (
      this.ranks.includes('4') &&
      this.ranks.includes('5') &&
      this.ranks.includes('6') &&
      this.ranks.includes('7') &&
      this.ranks.includes('A')
    ) || (
      this.ranks.includes('3') &&
      this.ranks.includes('4') &&
      this.ranks.includes('5') &&
      this.ranks.includes('6') &&
      this.ranks.includes('A')
    ) || (
      this.ranks.includes('2') &&
      this.ranks.includes('3') &&
      this.ranks.includes('4') &&
      this.ranks.includes('5') &&
      this.ranks.includes('A')
    );
  }

  isStraightFlush() {

  }


  isFullHouse() {
    const ranksNumber = {};

    this.ranks.forEach(rank => {
      if (!ranksNumber[rank]) {
        ranksNumber[rank] = 1;
      } else ranksNumber[rank]++;
    });

    return Object.values(ranksNumber).includes(3) &&
      Object.values(ranksNumber).includes(2);
  }

  getOutcome() {
    if (this.isRoyalFlush()) {
      return OUTCOMES.ROYAL_FLUSH;
    } else if (this.isFlush) {
      return OUTCOMES.FLUSH;
    } else if (this.isPair()) {
      return OUTCOMES.PAIR;
    } else if (this.isFour()) {
      return OUTCOMES.FOUR;
    } else if (this.isThree()) {
      return OUTCOMES.THREE;
    } else if (this.isStraight()) {
      return OUTCOMES.STRAIGHT;
    } else if (this.isStraightFlush()) {
      return OUTCOMES.STRAIGHT_FLUSH;
    } else if (this.isFullHouse()) {
      return OUTCOMES.FULL_HOUSE;
    } else {
      return OUTCOMES.NOTHING;
    }
  }
}

export default WinCalculator;